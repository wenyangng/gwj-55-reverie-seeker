@tool
extends MeshInstance3D

@export var gemColour:Color = Color("CFD3D9")
@export_range(0.0, 10.0, 0.1, "or_greater", "or_lesser") var gemAbsorption:float = 2.0

func _ready():
	var facetAvgPoints = PackedVector3Array()
	var facetNormals = PackedVector3Array()
	buildFacets(facetAvgPoints, facetNormals)
	get_active_material(0).set_shader_parameter("uRefractionIndex", 2.42)
	get_active_material(0).set_shader_parameter("totalOfFacets", facetAvgPoints.size())
	get_active_material(0).set_shader_parameter("facetPoints", facetAvgPoints)
	get_active_material(0).set_shader_parameter("facetNormals", facetNormals)
	get_active_material(0).set_shader_parameter(
		"uAbsorption", 
		Vector3(
			gemAbsorption * (1 - gemColour.r),
			gemAbsorption * (1 - gemColour.g),
			gemAbsorption * (1 - gemColour.b),
		)
	)

#func _process(delta):
#	if (gemColour.h > (35.0/360)) && (gemColour.h < (55.0/360)):
#		gemColour.h = gemColour.h + delta/50
#	else:
#		gemColour.h = fmod(gemColour.h + delta/10, 1.0)
#	get_active_material(0).set_shader_parameter(
#		"uAbsorption", 
#		Vector3(
#			gemAbsorption * (1 - gemColour.r),
#			gemAbsorption * (1 - gemColour.g),
#			gemAbsorption * (1 - gemColour.b),
#		)
#	)

func buildFacets(facetAvgPoints:PackedVector3Array, facetNormals:PackedVector3Array):
	var faceVertices = mesh.get_faces()
	for n in range(0, faceVertices.size(), 3):
		var knownFacet = false
		var point1 = faceVertices[n]
		var point2 = faceVertices[n + 1]
		var point3 = faceVertices[n + 2]
		for i in range(facetAvgPoints.size()):
			if isInPlane(facetAvgPoints[i], facetNormals[i], point1) \
				and isInPlane(facetAvgPoints[i], facetNormals[i], point2) \
				and isInPlane(facetAvgPoints[i], facetNormals[i], point3) :
				knownFacet = true;
				break;
				
		if !knownFacet:
			facetAvgPoints.append(computePlaneAvgPointFromTriangle(point1, point2, point3))
			facetNormals.append(computePlaneNormalFromTriangle(point1, point2, point3))

func isInPlane(planeAvgPoint:Vector3, normal:Vector3, point:Vector3) -> bool:
	var localCoords = point - planeAvgPoint;
	return abs(normal.dot(localCoords)) < 0.0001;
	
func computePlaneAvgPointFromTriangle(point1:Vector3, point2:Vector3, point3:Vector3) -> Vector3:
#	print(point1, ", ", point2, ", ", point3, ", ", (point1 + point2 + point3) / 3)
	return (point1 + point2 + point3) / 3

func computePlaneNormalFromTriangle(point1:Vector3, point2:Vector3, point3:Vector3) -> Vector3:
	var v12 = point2 - point1
	var v13 = point3 - point1
	var normal = v12.cross(v13).normalized() * -1
	return normal;
