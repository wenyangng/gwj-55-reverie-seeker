extends Node3D

@onready var skyTexture = get_node("%ColorRect")
@onready var dimStars = get_node("%StarParticles")
@onready var brightStars = get_node("%StarParticles2")
@onready var staticClouds = get_node("%StaticClouds")
@onready var movingClouds = get_node("%MovingClouds")
@onready var shootingStars = get_node("%ShootingStars")


func showDimStars():
	dimStars.show()
	pass

func showBrightStars():
	brightStars.show()
	pass

func showAurora():
	skyTexture.material.set_shader_parameter("showAurora", true)
	pass

func showHorizonLight():
	skyTexture.material.set_shader_parameter("showHorizon", true)
	pass
	
func showClouds():
	staticClouds.show()
	movingClouds.show()
	pass

func shotShootingStars():
	shootingStars.show()
	pass
