extends Node3D

@onready var sun:DirectionalLight3D = get_node("%Sun")
@onready var camera:Camera3D = get_node("%SunCamera")


@onready var meadow = get_node("%Meadow")
@onready var spirit = get_node("%SpiritParticles")
@onready var drizzle = get_node("%Drizzle")
@onready var rains = get_node("%Rains")
@onready var movingClouds = get_node("%MovingClouds")
@onready var flareTecture = get_node("%ColorRect")

func _process(delta):
	var effective_sun_direction: Vector3 = sun.global_transform.basis.z * maxf(camera.near, 1.0);
	effective_sun_direction += camera.global_transform.origin
	
#	visible = not camera.is_position_behind(effective_sun_direction)
	
#	if visible:
	var unprojected_sun_position: Vector2 = camera.unproject_position(effective_sun_direction)
	$ColorRect.material.set_shader_parameter("sun_position", unprojected_sun_position)
	pass


func showMeadow():
	meadow.show()
	pass

func showDrizzle():
	drizzle.show()
	pass

func showRains():
	rains.show()
	pass

func showSunFlare():
	flareTecture.material.set_shader_parameter("run", true)
	pass

func showSpirits():
	spirit.show()
	pass

func showCloud():
	movingClouds.show()
	pass
