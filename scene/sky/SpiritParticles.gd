@tool
extends GPUParticles3D

func _process(delta):
	if position.distance_to(Vector3.ZERO) > 2:
		position = position.lerp(Vector3.ZERO, delta)
	else:
		position = position.lerp(Vector3.LEFT * 3, delta)

#	var rotation_quaternion = Quaternion(Vector3.UP, delta)
#	position = rotation_quaternion * position
	pass
