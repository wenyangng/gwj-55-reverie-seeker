extends CanvasLayer

signal on_move_main_menu_camera

var sceneDayNight = "Night"

@onready var questionnaire= $HUD/Questionnaire

# Called when the node enters the scene tree for the first time.
func _ready():
	questionnaire.on_gem_selected.connect(on_gem_selected)
	questionnaire.on_day_night_selected.connect(on_day_night_selected)
	questionnaire.on_env_effect_selected.connect(on_env_effect_selected)
	questionnaire.on_spirit_animal_selected.connect(on_spirit_animal_selected)


func _on_main_menu_on_start_pressed():
	$HUD/MainMenu.visible = false
	emit_signal("on_move_main_menu_camera")
	$AudioStreamPlayer.play(0)
	#var weTween = $"3DScene/MainMenu/WorldEnvironment".create_tween()
	#weTween.tween_property($"3DScene/MainMenu/WorldEnvironment", "glow_strangth", 1, 3)
	#weTween.set_trans(Tween.TRANS_QUAD)


func _on_main_menu_camera_on_start_menu_finish_anim():
	if questionnaire.getCurrentQuestion() <= 1:
		questionnaire.visible = true
		questionnaire.reset()
		questionnaire.startQuestion()
	else:
		updateDayNightScene()
		

func on_gem_selected(diamondType: String, color: String):
	if OS.is_debug_build():
		print(diamondType + " " + color)
	if color == "":
		var env = get_node("%3DScene").get_node("%WorldEnvironment").environment
		create_tween().tween_property(env, "glow_strength", .7, 2) \
			.set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_OUT)
	else:
		var diamond_cut = load('res://models/diamond_' + diamondType.to_lower() + '.obj')
		var colourCode
		match color:
			"White":
				colourCode = Color.WHITE_SMOKE
			"Red":
				colourCode = Color.RED
			"Orange":
				colourCode = Color.ORANGE
			"Yellow":
				colourCode = Color.YELLOW
			"Green":
				colourCode = Color.GREEN
			"Cyan":
				colourCode = Color.CYAN
			"Blue":
				colourCode = Color.BLUE
			"Purple":
				colourCode = Color.PURPLE
		
		var diamond = get_node("%3DScene").get_node("%Diamond");
		var diamondSelected = get_node("%3DScene").get_node("%DiamondSelected");		
		var camera = get_node("%3DScene").get_node("%MainMenuCamera");
		camera.pause = true
		camera.move = false
		create_tween().tween_property(camera, "position", Vector3(0, 0, 1), 1) \
			.set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_OUT)
		create_tween().tween_property(camera, "rotation", Vector3(0, 0, 0), 1) \
			.set_trans(Tween.TRANS_LINEAR).set_ease(Tween.EASE_OUT)
		camera.pause = false
		diamond.visible = false
		diamondSelected.set_mesh(diamond_cut)
		diamondSelected.gemColour = colourCode
		diamondSelected.initDiamond()
		diamondSelected.visible = true
		
	pass

func on_day_night_selected(dayNight: String):
	emit_signal("on_move_main_menu_camera")
	sceneDayNight = dayNight
	
func updateDayNightScene():
	match sceneDayNight:
		"Day":
			var mainMenu = get_node("%3DScene").get_node("%MainMenu")
			var light1 = get_node("%3DScene").get_node("%FrontLight")
			var light2 = get_node("%3DScene").get_node("%BackLight")
			light1.hide()
			light2.hide()
			mainMenu.queue_free()
			var sceneTscn = load("res://scene/sky/vibrant_day.tscn")
			var scene = sceneTscn.instantiate()
			get_node("%3DScene").add_child(scene)
			pass
		"Night":
			var mainMenu = get_node("%3DScene").get_node("%MainMenu")
			mainMenu.queue_free()
			var sceneTscn = load("res://scene/sky/starry_night.tscn")
			var scene = sceneTscn.instantiate()
			get_node("%3DScene").add_child(scene)
			pass

func on_env_effect_selected(envEffect: String):
	if sceneDayNight == "Night":
		var sky = get_node("%3DScene").get_node("SkyBox")
		match envEffect:
			"Effect1":
				sky.showDimStars()
				pass
			"Effect2":
				sky.showBrightStars()
				pass
			"Effect3":
				sky.showAurora()
				pass
			"Effect4":
				sky.showHorizonLight()
				pass
			"Effect5":
				sky.showClouds()
				pass
			"Effect6":
				sky.shotShootingStars()
				pass
	elif sceneDayNight == "Day":
		var sky = get_node("%3DScene").get_node("SkyBox")
		match envEffect:
			"Effect1":
				sky.showDrizzle()
				pass
			"Effect2":
				sky.showRains()
				pass
			"Effect3":
				sky.showMeadow()
				pass
			"Effect4":
				sky.showSunFlare()
				pass
			"Effect5":
				sky.showCloud()
				pass
			"Effect6":
				sky.showSpirits()
				pass

func on_spirit_animal_selected(animal: String):
#	if sceneDayNight == "Night":
	var sky = get_node("%3DScene").get_node("SkyBox")
	match animal:
		"Whale":
			var sceneTscn = load("res://scene/model_emitted_particles/whale_mesh_particle.tscn")
			var scene = sceneTscn.instantiate()
			sky.add_child(scene)
			pass
		"Ladder":
			var sceneTscn = load("res://scene/model_emitted_particles/ladder_mesh_particle.tscn")
			var scene = sceneTscn.instantiate()
			sky.add_child(scene)
			pass
		"Hourglass":
			var sceneTscn = load("res://scene/model_emitted_particles/hourglass_mesh_particle.tscn")
			var scene = sceneTscn.instantiate()
			sky.add_child(scene)
			pass
		"Key":
			var sceneTscn = load("res://scene/model_emitted_particles/key_mesh_particle.tscn")
			var scene = sceneTscn.instantiate()
			sky.add_child(scene)
			pass

