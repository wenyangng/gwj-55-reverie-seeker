extends Camera3D

var camera_up = Vector3.UP
var speed = 0.002

var move = false
var pause = false
var notifiedEnd = false

signal on_start_menu_finish_anim

func _physics_process(_delta):
	if !pause:
		if move: 
			if position.distance_to(Vector3.ZERO) > 0.6:
				if OS.is_debug_build():
					position = position.lerp(Vector3.ZERO, _delta)
				else:	
					position = position.lerp(Vector3.ZERO, _delta/8)
				notifiedEnd = false
			elif notifiedEnd == false:
				notifiedEnd = true
				emit_signal("on_start_menu_finish_anim")

		var rotation_quaternion = Quaternion(Vector3.UP, speed) \
			* Quaternion(Vector3.RIGHT, speed) \
			* Quaternion(Vector3.FORWARD, speed) 
		var next_position = rotation_quaternion * position
		var plane_normal = (-1 * next_position).normalized()
		camera_up = camera_up - (camera_up.dot(plane_normal) * plane_normal)
		look_at_from_position(next_position, Vector3.ZERO, camera_up)

func move_into_gem():
	move = true

func _on_main_layout_on_move_main_menu_camera():
	move_into_gem()
