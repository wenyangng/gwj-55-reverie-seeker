extends Control

var currQ = 0
var qValueDict = {}

signal on_gem_selected
signal on_day_night_selected
signal on_env_effect_selected
signal on_spirit_animal_selected

func _on_pressed(question: int, value: String):
	$AudioStreamPlayer.play(0)
	qValueDict[question] = value
	
	if OS.is_debug_build():
		print("Q" + str(question) + ", answer=" + value)
	
	match question:
		1:
			emit_signal("on_gem_selected", "", "")			
		2:
			emit_signal("on_gem_selected", qValueDict[1], qValueDict[2])
		4:
			if qValueDict[3] == qValueDict[4]:
				emit_signal("on_day_night_selected", value)
			else:
				emit_signal("on_day_night_selected", qValueDict[3])
		5:
			emit_signal("on_env_effect_selected", value)
		6:
			emit_signal("on_env_effect_selected", value)
		7:
			emit_signal("on_env_effect_selected", value)
		8:
			emit_signal("on_env_effect_selected", value)
		9:
			emit_signal("on_spirit_animal_selected", value)
	
	showNextQuestion()

func startQuestion():
	currQ = 1
	animate_fadeIn($Q1, 0)

func getCurrentQuestion():
	return currQ

func reset():
	currQ = 0
	qValueDict.clear()
	
	$Q1.visible = false
	$Q2.visible = false
	$Q3.visible = false
	$Q4.visible = false
	$Q5.visible = false
	$Q6.visible = false
	$Q7.visible = false
	$Q8.visible = false
	$Q9.visible = false

func showNextQuestion():
	var currNode = get_node("Q" + str(currQ))
	animate_fadeOut(currNode, 0)

	if (currQ < 9):
		currQ = currQ + 1
		var delay = 0.5
		if currQ == 3:
			delay = 2.0
		elif currQ == 5:
			delay = 5
		
		var nextNode = get_node("Q" + str(currQ))
		animate_fadeIn(nextNode, delay)
	else:
		animate_fadeIn($CenterContainer, 5)
#		animate_fadeOut($CenterContainer, 10)

func animate_fadeOut(node: Node, delay: float):
	var tween = node.create_tween()
	tween.tween_property(node, "modulate", Color(1, 1, 1, 0), 2)\
		.set_trans(Tween.TRANS_LINEAR)\
		.set_ease(Tween.EASE_IN)\
		.set_delay(delay)
	node.visible = false

func animate_fadeIn(node: Node, delay: float):
	var tween = node.create_tween()
	node.modulate = Color(1, 1, 1, 0)
	node.visible = true
	tween.tween_property(node, "modulate", Color(1, 1, 1, 1), 2)\
		.set_trans(Tween.TRANS_LINEAR)\
		.set_ease(Tween.EASE_IN)\
		.set_delay(delay)

