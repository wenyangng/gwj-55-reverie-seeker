extends Control

signal on_start_pressed

func _on_start_button_pressed():
	$AudioStreamPlayer.play(0)
	emit_signal("on_start_pressed")

