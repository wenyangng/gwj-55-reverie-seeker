# Credits and Attributions are listed down in this itch.io project page https://carras-awakening.itch.io/reverie-seeker
  
# All the diamond models are modified by Carras on 2023-02-20 from the project diamond-webgl by piellardj (Jérémie Piellard) under GPLv3 https://github.com/piellardj/diamond-webgl
- ./models/diamond_brilliant.obj
- ./models/diamond_emerald.obj
- ./model/step_cut.obj (for testing use)
- ./model/step_diamond_normal.obj (for testing use)
- ./models/diamond_free_form.obj
- ./model/free_form_normal.obj (for testing use)
- ./models/diamond_oval.obj
- ./models/diamond_princess.obj
- ./models/diamond_triangle.obj